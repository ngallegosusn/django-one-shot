from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name"
        ]


class EditForm(ModelForm):
    class Meta:
        model: TodoList
        fields = [
            "name"
        ]
        
