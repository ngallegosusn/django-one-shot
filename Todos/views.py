from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, EditForm
# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect("todo_list_update", id=id)
    else:
        form = TodoListForm(instance=todos)
    context = {
            "form": form,
            "todo_detail": todos,
    }
    return render(request, "todos/modify.html", context)


def todo_list_delete(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
